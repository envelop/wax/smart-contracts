#include "./envelopatom1.hpp"

void envelopatom1::receive_asset(name from, name to, vector <uint64_t> asset_ids, string memo) {
    if (to != get_self()) {
        return;
    }

    check(asset_ids.size() == 1, "Only one NFT can be wrapped at a time");
    check(memo == "wrap", "Invalid memo");

    auto assets = atomicassets::get_assets(to);
    auto assets_itr = assets.find(asset_ids[0]);
    check(assets_itr != assets.end(), "Asset not found");

    auto blacklist_itr = _blacklist.find(assets_itr->collection_name.value);
    check(blacklist_itr == _blacklist.end(), "Collection is blacklisted for wrap");

    action(
        permission_level{ get_self(), name("active") },
        PROTOCOL_NAME,
        name("internalwrap"),
        make_tuple(
            from,
            get_self(),
            asset_ids[0]
        )
    ).send();
}

void envelopatom1::transfer(name to, uint64_t asset_id) {
    require_auth(PROTOCOL_NAME);

    vector <uint64_t> asset_ids = {asset_id};

    action(
        permission_level{ get_self(), name("active") },
        UNDERLINE_CONTRACT_NAME,
        name("transfer"),
        make_tuple(
            get_self(),
            to,
            asset_ids,
            std::string("")
        )
    ).send();
}

void envelopatom1::addblacklist(name collection_name) {
    require_auth(get_self());

    check(collection_name != name(""), "Enter collection name");

    auto blacklist_itr = _blacklist.find(collection_name.value);
    check(blacklist_itr == _blacklist.end(), "Collection already in blacklist");

    _blacklist.emplace(get_self(), [&](auto& row) {
        row.collection_name = collection_name;
    });
}

void envelopatom1::delblacklist(name collection_name) {
    require_auth(get_self());

    check(collection_name != name(""), "Enter collection name");

    auto blacklist_itr = _blacklist.find(collection_name.value);
    check(blacklist_itr != _blacklist.end(), "Collection not in blacklist");

    _blacklist.erase(blacklist_itr);
}
