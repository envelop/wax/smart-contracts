#include "envelopwrap1.hpp"

void envelopwrap1::init() {
    require_auth(get_self());
    _config.get_or_create(get_self(), config_s{});
}

/**
* Internal function used only by layer_contract. Called during NFT transfer to layer_contract
*/
void envelopwrap1::internalwrap(name owner, name layer_contract, uint64_t token_id) {
    require_auth(layer_contract);

    auto layers_itr = _layers.find(layer_contract.value);
    check(layers_itr != _layers.end(), "Layer not found");

    config_s current_config = _config.get();
    uint64_t wrapped_token_id = ++current_config.wrapped_token_counter;
    _config.set(current_config, get_self());

    _tokens.emplace(get_self(), [&](auto& row) {
        row.wrapped_token_id = wrapped_token_id;
        row.layer_contract = layer_contract;
        row.token_id = token_id;
        row.owner = owner;
    });
}

/**
* Wrapping NFT transferred to the layer_contract. Called by NFT owner

* @params:
*   wrapped_token_id - token id for wrapping
*   unwrap_after - timestamp in seconds after which the wNFT can be unwrapped, 0 if unused
*   royalty_beneficiary - account receiving royalty, "" if unused
*   transfer_fee - the amount of the transfer fee, for example "1.00000000 WAX" or "0 WAX"
*   royalty_percent - percentage of royalties from the transfer fee
*   unwrapt_fee_threshold - the minimum amount of the accumulated transfer fee, after which you can unwrap wNFT
*/
void envelopwrap1::wrap(
    uint64_t wrapped_token_id, 
    uint64_t unwrap_after, 
    name royalty_beneficiary,
    asset transfer_fee,
    uint8_t royalty_percent, 
    uint64_t unwrapt_fee_threshold
) {
    auto tokens_itr = _tokens.find(wrapped_token_id);
    check(tokens_itr != _tokens.end(), "Token not found");
    check(!tokens_itr->wrapped, "Token already wrapped");
    require_auth(tokens_itr->owner);

    config_s current_config = _config.get();
    if (unwrap_after > 0) {
        uint32_t now = eosio::current_time_point().sec_since_epoch();
        check(unwrap_after > now, "Unwrap time must be greater then now or equal to 0");
        check(unwrap_after - now <= current_config.max_time_to_unwrap, "Too long Wrap");
    }

    asset accumulated_fee = asset();
    if (transfer_fee.amount > 0) {
        auto _ptokens_itr = _ptokens.find(transfer_fee.symbol.raw());
        check(_ptokens_itr != _ptokens.end(), "This transferFee token is not enabled");

        if (royalty_beneficiary != name("")) {
            check(is_account(royalty_beneficiary), "Royalty beneficiary account does not exist");
            check(royalty_percent <= current_config.max_royalty_percent, "Royalty percent too big");
            check(royalty_percent > 0, "Royalty percent must be greather then 0");
        }
        else {
            check(royalty_percent == 0, "Royalty percent must be equal to 0");
        }

        check(transfer_fee.amount <= _ptokens_itr->max_supply.amount * current_config.max_fee_threshold_percent / 100, "Too much transfer fee");
        check(unwrapt_fee_threshold <= _ptokens_itr->max_supply.amount * current_config.max_fee_threshold_percent / 100, "Too much threshold");

        accumulated_fee.symbol = transfer_fee.symbol;
    }
    else {
        transfer_fee = asset();
        check(royalty_beneficiary == name(""), "No Royalty without transferFee");
        check(royalty_percent == 0, "Royalty source is transferFee");
        check(unwrapt_fee_threshold == 0, "Cant set Threshold without transferFee");
    }

    _tokens.modify(tokens_itr, get_self(), [&](auto& row) {
        row.unwrap_after = unwrap_after;
        row.royalty_beneficiary = royalty_beneficiary;
        row.wrapped = true;
        row.transfer_fee = transfer_fee;
        row.royalty_percent = royalty_percent;
        row.unwrapt_fee_threshold = unwrapt_fee_threshold;
        row.accumulated_fee = accumulated_fee;
    });
}

/**
* Unwrapping wNFT
*/
void envelopwrap1::unwrap(uint64_t wrapped_token_id) {
    auto tokens_itr = check_wrapped(wrapped_token_id);
    check(tokens_itr->unwrapt_fee_threshold <= tokens_itr->accumulated_fee.amount, "Cant unwrap due Fee Threshold");

    require_auth(tokens_itr->owner);

    auto layers_itr = _layers.find(tokens_itr->layer_contract.value);
    check(layers_itr != _layers.end(), "Layer not found");

    if (tokens_itr->unwrap_after > 0) {
        check(tokens_itr->unwrap_after < eosio::current_time_point().sec_since_epoch(), "Cant unwrap before day X");
    }

    if (tokens_itr->accumulated_fee.amount > 0) {
        add_balance(tokens_itr->owner, tokens_itr->accumulated_fee);
    }

    collateral_t _collaterals(get_self(), wrapped_token_id);
    auto collaterals_itr = _collaterals.begin();
    while (collaterals_itr != _collaterals.end()) {
        add_balance(tokens_itr->owner, collaterals_itr->quantity);
        _collaterals.erase(collaterals_itr);
        collaterals_itr = _collaterals.begin();
    }

    notify_approvals(tokens_itr->owner);

    action(
        permission_level{ get_self(), name("active") },
        tokens_itr->layer_contract,
        name("transfer"),
        make_tuple(
            tokens_itr->owner,
            tokens_itr->token_id
        )
    ).send();

    _tokens.erase(tokens_itr);
}

/**
* Transferring wNFT
*/
void envelopwrap1::transfer(uint64_t wrapped_token_id, name to) {
    auto tokens_itr = check_wrapped(wrapped_token_id);
    check(tokens_itr->owner != to, "Can't transfer to self");

    require_auth(tokens_itr->owner);

    _transfer(tokens_itr, to);
}

/**
* Transferring someone else's wNFT. Available only for approved accounts. Called by sender
*/
void envelopwrap1::transferfrom(uint64_t wrapped_token_id, name to, name sender) {
    auto tokens_itr = check_wrapped(wrapped_token_id);
    check(tokens_itr->owner != to, "Can't transfer to self");

    check(is_approved_or_owner(sender, wrapped_token_id), "You have not permission");

    _transfer(tokens_itr, to);
}

/**
* Internal function for transferring
*/
void envelopwrap1::_transfer(envelopwrap1::token_t::const_iterator tokens_itr, name to) {
    check(is_account(to), "Receiver account does not exist");

    asset accumulated_fee = tokens_itr->accumulated_fee;
    if (tokens_itr->transfer_fee.amount > 0) {
        sub_balance(tokens_itr->owner, tokens_itr->transfer_fee);
        asset transfer_fee = tokens_itr->transfer_fee;
        if (tokens_itr->royalty_percent > 0) {
            asset royalty_amount = transfer_fee * tokens_itr->royalty_percent / 100;
            add_balance(tokens_itr->royalty_beneficiary, royalty_amount);
            transfer_fee -= royalty_amount;
        }
        accumulated_fee += transfer_fee;
    }

    notify_approvals(tokens_itr->owner);

    _tokens.modify(tokens_itr, get_self(), [&](auto& row) {
        row.owner = to;
        row.accumulated_fee = accumulated_fee;
        row.token_approvals = name("");
    });
}

/**
* Return of not wrapped NFT to the owner. Called by owner
*/
void envelopwrap1::transferback(uint64_t wrapped_token_id) {
    auto tokens_itr = _tokens.find(wrapped_token_id);
    check(tokens_itr != _tokens.end(), "Token not found");
    check(!tokens_itr->wrapped, "Token already wrapped");

    require_auth(tokens_itr->owner);

    action(
        permission_level{ get_self(), name("active") },
        tokens_itr->layer_contract,
        name("transfer"),
        make_tuple(
            tokens_itr->owner,
            tokens_itr->token_id
        )
    ).send();

    _tokens.erase(tokens_itr);
}

/**
* Depositing tokens to the user's balance in the Protocol. Called by other contracts ("*::transfer")
*/
void envelopwrap1::deposit(name from, name to, asset quantity, string memo)
{
    if (to != get_self()) {
        return;
    }

    if (memo == "donate") {
        return;
    }

    auto ptokens_itr = _ptokens.find(quantity.symbol.raw());
    check(ptokens_itr != _ptokens.end(), "Token not available for deposit");
    check(get_first_receiver() == ptokens_itr->partner_token_contract, "Wrong contract");

    check(quantity.amount > 0, "Amount must be greater then 0");

    add_balance(from, quantity);
}

/**
*  Depositing WAX to the user's balance in the Protocol. Called by other contracts ("eosio.token::transfer")
*/
void envelopwrap1::onwaxdeposit(name from, name to, asset quantity, string memo)
{
    deposit(from, to, quantity, memo);
}

/**
* Withdrawing token from the user's balance in the Protocol
*/
void envelopwrap1::withdraw(name owner, asset quantity)
{
    require_auth(owner);

    auto ptokens_itr = _ptokens.find(quantity.symbol.raw());
    check(ptokens_itr != _ptokens.end(), "Token not available for withdraw");

    check(quantity.amount > 0, "Amount must be greater than 0");

    sub_balance(owner, quantity);

    action(
        permission_level{ get_self(), name("active") },
        ptokens_itr->partner_token_contract,
        name("transfer"),
        make_tuple(
            get_self(),
            owner,
            quantity,
            std::string("")
        )
    ).send();
}

/**
* Adding tokens to wNFT's collateral from the user's balance in the Protocol. The user can add collateral to any wNFT
*/
void envelopwrap1::tocollateral(name username, uint64_t wrapped_token_id, asset quantity) {
    require_auth(username);

    auto tokens_itr = check_wrapped(wrapped_token_id);

    auto ptokens_itr = _ptokens.find(quantity.symbol.raw());
    check(ptokens_itr != _ptokens.end(), "Token not available for collateral");

    check(quantity.amount > 0, "Amount must be greater then 0");

    sub_balance(username, quantity);

    add_collateral(wrapped_token_id, quantity);
}

/**
* Deleting layer_contract from the Protocol. Called by get_self()
*/
void envelopwrap1::dellayer(name layer_contract) {
    require_auth(get_self());

    auto itr = _layers.find(layer_contract.value);
    check(itr != _layers.end(), "Layer not found");

    _layers.erase(itr);
}

/**
* Setting layer_contract in the Protocol. Called by get_self()
*/
void envelopwrap1::setlayer(name layer_contract, name underline_contract) {
    require_auth(get_self());

    check(is_account(underline_contract), "Underline account does not exist");
    check(is_account(layer_contract), "Layer account does not exist");

    auto itr = _layers.find(layer_contract.value);
    if (itr != _layers.end()) {
        _layers.modify(itr, get_self(), [&](auto& row) {
            row.underline_contract = underline_contract;
        });
    }
    else {
        _layers.emplace(get_self(), [&](auto& row) {
            row.layer_contract = layer_contract;
            row.underline_contract = underline_contract;
        });
    }
}

/**
* Adding partner tokens to the Protocol. Called by get_self()
*/
void envelopwrap1::addptoken(asset max_supply, name partner_token_contract) {
    require_auth(get_self());

    check(is_account(partner_token_contract), "Token account does not exist");

    auto ptokens_itr = _ptokens.find(max_supply.symbol.raw());
    check(ptokens_itr == _ptokens.end(), "Token already exist");
    _ptokens.emplace(get_self(), [&](auto& row) {
        row.max_supply = max_supply;
        row.partner_token_contract = partner_token_contract;
    });
}

/**
* Deleting partner tokens from the Protocol. Called by get_self()
*/
void envelopwrap1::delptoken(symbol token_symbol) {
    require_auth(get_self());

    auto ptokens_itr = _ptokens.find(token_symbol.raw());
    check(ptokens_itr != _ptokens.end(), "Token not found");

    _ptokens.erase(ptokens_itr);
}

/**
* Approving account to the wNFT. Called by wNFT owner

* @params:
*   to - approving account
*   wrapped_token_id - wNFT id
*/
void envelopwrap1::approve(name to, uint64_t wrapped_token_id) {
    auto tokens_itr = check_wrapped(wrapped_token_id);
    check(tokens_itr->owner != to, "Can't approve to self");

    require_auth(tokens_itr->owner);

    check(is_account(to) || to == name(""), "Approved account does not exist");

    _tokens.modify(tokens_itr, get_self(), [&](auto& row) {
        row.token_approvals = to;
    });
}

/**
* Approving account to all owner's wNFTs. Called by owner

* @params:
*   owner - wNFT owner
*   to - approving account
*   approved - "true" to add approval, "false" to delete
*/
void envelopwrap1::approveall(name owner, name to, bool approved) {
    require_auth(owner);

    check(owner != to, "Can't approve to self");

    check(is_account(to), "Approved account does not exist");

    approval_t _approvals(get_self(), owner.value);
    auto approvals_itr = _approvals.find(to.value);

    if (approved) {
        check(approvals_itr == _approvals.end(), "Already approved");

        _approvals.emplace(owner, [&](auto& row) {
            row.approval = to;
        });
    }
    else {
        check(approvals_itr != _approvals.end(), "Already not approved");

        _approvals.erase(approvals_itr);
    }
}

void envelopwrap1::add_balance(name owner, asset quantity) {
    balance_t _balances(get_self(), owner.value);
    auto balances_itr = _balances.find(quantity.symbol.raw());
    if (balances_itr == _balances.end()) {
        _balances.emplace(get_self(), [&](auto& row) {
            row.quantity = quantity;
        });
    }
    else {
        _balances.modify(balances_itr, get_self(), [&](auto& row) {
            row.quantity += quantity;
        });
    }
}

void envelopwrap1::sub_balance(name owner, asset quantity) {
    balance_t _balances(get_self(), owner.value);
    auto balances_itr = _balances.find(quantity.symbol.raw());
    check(balances_itr != _balances.end(), "Balance not found");
    check(balances_itr->quantity.amount >= quantity.amount, "Insufficient balance");

    _balances.modify(balances_itr, get_self(), [&](auto& row) {
        row.quantity -= quantity;
    });
}

void envelopwrap1::add_collateral(uint64_t wrapped_token_id, asset quantity) {
    collateral_t _collaterals(get_self(), wrapped_token_id);
    auto collaterals_itr = _collaterals.find(quantity.symbol.raw());
    if (collaterals_itr == _collaterals.end()) {
        _collaterals.emplace(get_self(), [&](auto& row) {
            row.quantity = quantity;
        });
    }
    else {
        _collaterals.modify(collaterals_itr, get_self(), [&](auto& row) {
            row.quantity += quantity;
        });
    }
}

/**
* Checking token for the wrapping
*/
envelopwrap1::token_t::const_iterator envelopwrap1::check_wrapped(uint64_t wrapped_token_id) {
    auto tokens_itr = _tokens.find(wrapped_token_id);
    check(tokens_itr != _tokens.end(), "Token not found");
    check(tokens_itr->wrapped, "Token not wrapped yet");
    return tokens_itr;
}

/**
* Checking access to the wNFT
*/
bool envelopwrap1::is_approved_or_owner(name sender, uint64_t wrapped_token_id) {
    auto tokens_itr = _tokens.find(wrapped_token_id);
    check(tokens_itr != _tokens.end(), "Token not found");

    approval_t _approvals(get_self(), tokens_itr->owner.value);
    auto approvals_itr = _approvals.find(sender.value);
    if (tokens_itr->owner == sender || tokens_itr->token_approvals == sender || approvals_itr != _approvals.end()) {
        return has_auth(sender);
    }
    
    return false;
}

/**
* Notification of the account for some actions with the wNFT
*/
void envelopwrap1::notify_approvals(name owner) {
    approval_t _approvals(get_self(), owner.value);
    for (auto approvals_itr = _approvals.begin(); approvals_itr != _approvals.end(); approvals_itr++) {
        require_recipient(approvals_itr->approval);
    }
}
