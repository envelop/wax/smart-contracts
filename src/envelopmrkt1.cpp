#include "envelopmrkt1.hpp"

void envelopmrkt1::init() {
    require_auth(get_self());
    _config.get_or_create(get_self(), config_s{});
}

/*
* Creating offer
*/
void envelopmrkt1::createoffer(name seller, uint64_t wrapped_token_id, asset price) {
    require_auth(seller);

    auto by_token = _offers.get_index<"bytoken"_n>();
    auto by_token_itr = by_token.find(wrapped_token_id);
    check(by_token_itr == by_token.end(), "Offer with this wrapped_token_id already exists");

    check(price.amount > 0, "Price must be greater than 0");
    check(price.symbol == symbol("WAX", 8), "Price must be in WAX with precision == 8");

    auto tokens_itr = envelopwrap1::_tokens.find(wrapped_token_id);
    check(tokens_itr != envelopwrap1::_tokens.end(), "Token not found");
    check(tokens_itr->owner == seller, "This is not your token");
    check(tokens_itr->wrapped, "This token not wrapped yet");

    check(check_approvals(seller), "Marketplace have not permissions to your tokens");

    config_s current_config = _config.get();
    uint64_t offer_id = ++current_config.offer_counter;
    _config.set(current_config, get_self());

    _offers.emplace(seller, [&](auto& row) {
        row.offer_id = offer_id;
        row.wrapped_token_id = wrapped_token_id;
        row.seller = seller;
        row.price = price;
    });
}

/*
* Canceling offer
*/
void envelopmrkt1::canceloffer(uint64_t offer_id) {
    auto offer_itr = _offers.find(offer_id);
    check(offer_itr != _offers.end(), "Offer not found");

    require_auth(offer_itr->seller);

    _offers.erase(offer_itr);
}

/*
* Removing offer when wNFT transferred
*/
void envelopmrkt1::token_transfer(uint64_t wrapped_token_id, name to) {
    remove_offer_by_token_id(wrapped_token_id);
}

/*
* Removing offer when wNFT transferred
*/
void envelopmrkt1::token_transferfrom(uint64_t wrapped_token_id, name to, name sender) {
    remove_offer_by_token_id(wrapped_token_id);
}

/*
* Removing offer when wNFT unwrapped
*/
void envelopmrkt1::token_unwrap(uint64_t wrapped_token_id) {
    remove_offer_by_token_id(wrapped_token_id);
}

/*
* Inrernal function for removing
*/
void envelopmrkt1::remove_offer_by_token_id(uint64_t wrapped_token_id) {
    auto by_token = _offers.get_index<"bytoken"_n>();
    auto by_token_itr = by_token.find(wrapped_token_id);
    if (by_token_itr != by_token.end()) {
        by_token.erase(by_token_itr);
    }
}
    
/*
* Buying offer. Called by eosio.token when WAX transferred to contract
*/
void envelopmrkt1::onwaxdeposit(name from, name to, asset quantity, string memo)
{
    if (to != get_self()) {
        return;
    }

    if (memo == "donate") {
        return;
    }

    vector<string> memo_vec = split(memo, " ");
    if (memo_vec.at(0) == "fill") {
        check(memo_vec.size() == 2, "Wrong memo");

        uint64_t offer_id = stoull(memo_vec.at(1));
        auto offers_itr = _offers.find(offer_id);
        check(offers_itr != _offers.end(), "Offer not found");
        check(offers_itr->seller != from, "Can't buy from youself");
        check(offers_itr->price == quantity, "Wrong price");

        auto tokens_itr = envelopwrap1::_tokens.find(offers_itr->wrapped_token_id);
        check(tokens_itr != envelopwrap1::_tokens.end(), "Token not found");
        check(tokens_itr->owner == offers_itr->seller, "Seller have not this token");

        check(check_approvals(offers_itr->seller), "Marketplace have not permissions to transfer token");
        
        action(
            permission_level{ get_self(), name("active") },
            ENVELOP_PROTOCOL,
            name("transferfrom"),
            make_tuple(
                offers_itr->wrapped_token_id,
                from,
                get_self()
            )
        ).send();

        action(
            permission_level{ get_self(), name("active") },
            name("eosio.token"),
            name("transfer"),
            make_tuple(
                get_self(),
                offers_itr->seller,
                quantity,
                std::string("")
            )
        ).send();

        _offers.erase(offers_itr);
    }
    else {
        check(false, "Wrong memo");
    }
}

vector<string> envelopmrkt1::split(string s, string delimiter) {
    size_t pos_start = 0, pos_end, delim_len = delimiter.length();
    string token;
    vector<string> res;

    while ((pos_end = s.find(delimiter, pos_start)) != string::npos) {
        token = s.substr(pos_start, pos_end - pos_start);
        pos_start = pos_end + delim_len;
        res.push_back(token);
    }

    res.push_back(s.substr(pos_start));
    return res;
}

bool envelopmrkt1::check_approvals(name owner) {
    auto approvals = envelopwrap1::get_approvals(owner);
    auto approvals_itr = approvals.find(get_self().value);
    return (approvals_itr != approvals.end());
}
