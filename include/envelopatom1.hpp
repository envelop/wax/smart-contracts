#include <eosio/eosio.hpp>
#include "./atomicassets-interface.hpp"

using namespace std;
using namespace eosio;

name PROTOCOL_NAME = name("envelopwrap1");
name UNDERLINE_CONTRACT_NAME = name("atomicassets");

class [[eosio::contract]] envelopatom1 : public eosio::contract {

private:

    struct [[eosio::table]] blacklist_s {
        name            collection_name;

        auto primary_key() const { return collection_name.value; }
    };
    typedef eosio::multi_index<name("blacklist"), blacklist_s> blacklist_t;

    blacklist_t _blacklist = blacklist_t(get_self(), get_self().value);

public:

    envelopatom1(name receiver, name code, datastream<const char*> ds) :contract(receiver, code, ds) {}

    [[eosio::on_notify("atomicassets::transfer")]] void receive_asset(
        name from,
        name to,
        vector <uint64_t> asset_ids,
        string memo
    );

    [[eosio::action]]
    void transfer(name to, uint64_t asset_id);

    [[eosio::action]]
    void addblacklist(name collection_name);

    [[eosio::action]]
    void delblacklist(name collection_name);
};