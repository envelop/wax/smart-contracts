#include <eosio/eosio.hpp>
#include <eosio/singleton.hpp>
#include <eosio/asset.hpp>
#include "./envelopwrap1-interface.hpp"

using namespace std;
using namespace eosio;

class [[eosio::contract]] envelopmrkt1 : public eosio::contract {

private:

    struct [[eosio::table]] offer_s {
        uint64_t        offer_id;
        uint64_t        wrapped_token_id;
        name            seller;
        asset           price;

        auto primary_key() const { return offer_id; }
        uint64_t get_by_token() const { return wrapped_token_id; }
        uint128_t get_by_seller() const { return ((uint128_t)seller.value << 64) | (uint128_t)offer_id; }
    };
    typedef eosio::multi_index<
        name("offers"), 
        offer_s, 
        indexed_by<name("bytoken"), const_mem_fun<offer_s, uint64_t, &offer_s::get_by_token>>, 
        indexed_by<name("byseller"), const_mem_fun<offer_s, uint128_t, &offer_s::get_by_seller>>
    > offer_t;

    struct [[eosio::table]] config_s {
        uint64_t        offer_counter = 0;
    };
    typedef singleton <name("config"), config_s> config_t;
    //https://github.com/EOSIO/eosio.cdt/issues/280
    typedef multi_index <name("config"), config_s> config_t_for_abi;

    offer_t _offers = offer_t(get_self(), get_self().value);
    config_t _config = config_t(get_self(), get_self().value);

    void remove_offer_by_token_id(uint64_t wrapped_token_id);
    vector<string> split(string s, string delimiter);
    bool check_approvals(name owner);

public:

    envelopmrkt1(name receiver, name code, datastream<const char*> ds) :contract(receiver, code, ds) {}

    [[eosio::on_notify("envelopwrap1::transfer")]]
    void token_transfer(uint64_t wrapped_token_id, name to);

    [[eosio::on_notify("envelopwrap1::transferfrom")]]
    void token_transferfrom(uint64_t wrapped_token_id, name to, name sender);

    [[eosio::on_notify("envelopwrap1::unwrap")]]
    void token_unwrap(uint64_t wrapped_token_id);

    [[eosio::on_notify("eosio.token::transfer")]]
    void onwaxdeposit(name from, name to, asset quantity, string memo);

    [[eosio::action]]
    void init();

    [[eosio::action]]
    void createoffer(name seller, uint64_t wrapped_token_id, asset price);

    [[eosio::action]]
    void canceloffer(uint64_t offer_id);
};