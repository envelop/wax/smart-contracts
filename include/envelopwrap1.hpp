#include <eosio/eosio.hpp>
#include <eosio/singleton.hpp>
#include <eosio/asset.hpp>

using namespace std;
using namespace eosio;

class [[eosio::contract]] envelopwrap1 : public eosio::contract {

private:

    struct [[eosio::table]] token_s {
        uint64_t        wrapped_token_id;
        name            layer_contract;
        uint64_t        token_id;   //original token id
        name            owner;
        name            royalty_beneficiary;
        uint64_t        unwrap_after = 0;   //timestamp in sec
        bool            wrapped = false;    //wrapped status
        asset           transfer_fee = asset();
        uint8_t         royalty_percent = 0;    //royalty for creator
        uint64_t        unwrapt_fee_threshold = 0;  //amount of min accumulated fee for unwrap (int with decimals)
        asset           accumulated_fee = asset();
        name            token_approvals = name("");

        auto primary_key() const { return wrapped_token_id; }
        uint128_t get_by_owner() const { return ((uint128_t)owner.value << 64) | (uint128_t)wrapped_token_id; }
    };
    typedef eosio::multi_index<name("tokens"), token_s, indexed_by<name("byowner"), const_mem_fun<token_s, uint128_t, &token_s::get_by_owner>>> token_t;

    struct [[eosio::table]] layer_s {
        name            layer_contract; //layer name for NFT
        name            underline_contract; //origin NFT contract name

        auto primary_key() const { return layer_contract.value; }
    };
    typedef eosio::multi_index<name("layers"), layer_s> layer_t;

    struct [[eosio::table]] partner_token_s {
        asset           max_supply;
        name            partner_token_contract;

        auto primary_key() const { return max_supply.symbol.raw(); }
    };
    typedef eosio::multi_index<name("ptokens"), partner_token_s> partner_token_t;

    //Scope: owner
    struct [[eosio::table]] balance_s {
        asset    quantity;

        uint64_t primary_key() const { return quantity.symbol.raw(); }
    };
    typedef eosio::multi_index<name("balances"), balance_s> balance_t;

    //Scope: wrapped_token_id
    struct [[eosio::table]] collateral_s {
        asset    quantity;

        uint64_t primary_key() const { return quantity.symbol.raw(); }
    };
    typedef eosio::multi_index<name("collaterals"), collateral_s> collateral_t;

    //Scope: owner
    struct [[eosio::table]] approval_s {
        name    approval;

        uint64_t primary_key() const { return approval.value; }
    };
    typedef eosio::multi_index<name("approvals"), approval_s> approval_t;

    struct [[eosio::table]] config_s{
        uint64_t        wrapped_token_counter = 0;
        uint32_t        max_time_to_unwrap = 31536000;
        uint8_t         max_royalty_percent = 50;
        uint8_t         max_fee_threshold_percent = 1;
    };
    typedef singleton <name("config"), config_s> config_t;
    //https://github.com/EOSIO/eosio.cdt/issues/280
    typedef multi_index <name("config"), config_s> config_t_for_abi;

    token_t _tokens = token_t(get_self(), get_self().value);
    layer_t _layers = layer_t(get_self(), get_self().value);
    partner_token_t _ptokens = partner_token_t(get_self(), get_self().value);
    config_t _config = config_t(get_self(), get_self().value);

    void add_balance(name owner, asset quantity);
    void sub_balance(name owner, asset quantity);
    void add_collateral(uint64_t wrapped_token_id, asset quantity);
    void _transfer(token_t::const_iterator tokens_itr, name to);
    token_t::const_iterator check_wrapped(uint64_t wrapped_token_id);
    bool is_approved_or_owner(name sender, uint64_t wrapped_token_id);
    void notify_approvals(name owner);

public:

    envelopwrap1(name receiver, name code, datastream<const char*> ds) :contract(receiver, code, ds) {}

    [[eosio::action]]
    void init();

    [[eosio::action]]
    void internalwrap(name owner, name layer_contract, uint64_t token_id);

    [[eosio::action]]
    void wrap(
        uint64_t wrapped_token_id,
        uint64_t unwrap_after,
        name royalty_beneficiary,
        asset transfer_fee,
        uint8_t royalty_percent,
        uint64_t unwrapt_fee_threshold
    );

    [[eosio::action]]
    void unwrap(uint64_t wrapped_token_id);

    [[eosio::action]]
    void transfer(uint64_t wrapped_token_id, name to);

    [[eosio::action]]
    void transferfrom(uint64_t wrapped_token_id, name to, name sender);

    [[eosio::action]]
    void transferback(uint64_t wrapped_token_id);
        
    [[eosio::on_notify("*::transfer")]]
    void deposit(name from, name to, asset quantity, string memo);

    [[eosio::on_notify("eosio.token::transfer")]]
    void onwaxdeposit(name from, name to, asset quantity, string memo);

    [[eosio::action]]
    void withdraw(name owner, asset quantity);

    [[eosio::action]]
    void tocollateral(name username, uint64_t wrapped_token_id, asset quantity);

    [[eosio::action]]
    void dellayer(name layer_contract);

    [[eosio::action]]
    void setlayer(name layer_contract, name underline_contract);

    [[eosio::action]]
    void addptoken(asset max_supply, name partner_token_contract);

    [[eosio::action]]
    void delptoken(symbol token_symbol);

    [[eosio::action]]
    void approve(name to, uint64_t wrapped_token_id);

    [[eosio::action]]
    void approveall(name owner, name to, bool approved);
};