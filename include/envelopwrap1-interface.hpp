#include <eosio/eosio.hpp>
#include <eosio/singleton.hpp>
#include <eosio/asset.hpp>

using namespace eosio;
using namespace std;

static constexpr name ENVELOP_PROTOCOL = name("envelopwrap1");

namespace envelopwrap1 {

    struct [[eosio::table]] token_s {
        uint64_t        wrapped_token_id;
        name            layer_contract;
        uint64_t        token_id;   //original token id
        name            owner;
        name            royalty_beneficiary;
        uint64_t        unwrap_after = 0;   //timestamp in sec
        bool            wrapped = false;    //wrapped status
        asset           transfer_fee = asset();
        uint8_t         royalty_percent = 0;    //royalty for creator
        uint64_t        unwrapt_fee_threshold = 0;  //amount of min accumulated fee for unwrap (int with decimals)
        asset           accumulated_fee = asset();
        name            token_approvals = name("");

        auto primary_key() const { return wrapped_token_id; }
        uint64_t get_by_owner() const { return owner.value; }
    };
    typedef eosio::multi_index<name("tokens"), token_s, indexed_by<name("byowner"), const_mem_fun<token_s, uint64_t, &token_s::get_by_owner>>> token_t;

    //Scope: owner
    struct [[eosio::table]] approval_s {
        name    approval;

        uint64_t primary_key() const { return approval.value; }
    };
    typedef eosio::multi_index<name("approvals"), approval_s> approval_t;

    token_t _tokens = token_t(ENVELOP_PROTOCOL, ENVELOP_PROTOCOL.value);

    approval_t get_approvals(name acc) {
        return approval_t(ENVELOP_PROTOCOL, acc.value);
    }
};
